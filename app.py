from pyrogram import Client, Filters, ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from mtranslate import translate
from gtts import gTTS


app = Client('your-bot-token')


def text2speech(msg):
    tts = gTTS(text=msg)
    tts.save(voive)

@app.on_message(Filters.command(["start"]))
def my_handler(client, message):
    client.send_chat_action(message.chat.id, 'typing')
    client.send_message(message.chat.id,'Hello, ' + message.from_user.first_name+".\nI'm Language Translator Bot.I can translate any language to any language.\nTo know how to use me use /help command.")

@app.on_message(Filters.command(["help"]))
def my_handler(client, message):
    client.send_chat_action(message.chat.id, 'typing')
    client.send_message(message.chat.id,'Hello, ' + message.from_user.first_name+".\nYou may send me any text that you need to translate.\n Please follow the following format.\n<text to translate> | <language to which text be translated>\n\nEg: Hi how are you | ar\nUse /list to know language codes.\n\n\n__If you do not specify any language code, the given text will be translated to English.__")

@app.on_message(Filters.command(["list"]))
def my_handler(client, message):
    client.send_chat_action(message.chat.id, 'typing')
    client.send_message(message.chat.id, "'af': 'afrikaans'\n'sq': 'albanian'\n'am': 'amharic'\n'ar': 'arabic'\n'hy': 'armenian'\n'az': 'azerbaijani'\n'eu': 'basque'\n'be': 'belarusian'\n'bn': 'bengali'\n'bs': 'bosnian'\n'bg': 'bulgarian'\n'ca': 'catalan'\n'ceb': 'cebuano'\n'ny': 'chichewa'\n'zh-cn': 'chinese (simplified)'\n'zh-tw': 'chinese (traditional)'\n'co': 'corsican'\n'hr': 'croatian'\n'cs': 'czech'\n'da': 'danish'\n'nl': 'dutch'\n'en': 'english'\n'eo': 'esperanto'\n'et': 'estonian'\n'tl': 'filipino'\n'fi': 'finnish'\n'fr': 'french'\n'fy': 'frisian'\n'gl': 'galician'\n'ka': 'georgian'\n'de': 'german'\n'el': 'greek'\n'gu': 'gujarati'\n'ht': 'haitian creole'\n'ha': 'hausa'\n'haw': 'hawaiian'\n'iw': 'hebrew'\n'hi': 'hindi'\n'hmn': 'hmong'\n'hu': 'hungarian'\n'is': 'icelandic'\n'ig': 'igbo'\n'id': 'indonesian'\n'ga': 'irish'\n'it': 'italian'\n'ja': 'japanese'\n'jw': 'javanese'\n'kn': 'kannada'\n'kk': 'kazakh'\n'km': 'khmer'\n'ko': 'korean'\n'ku': 'kurdish (kurmanji)'\n'ky': 'kyrgyz'\n'lo': 'lao'\n'la': 'latin'\n'lv': 'latvian'\n'lt': 'lithuanian'\n'lb': 'luxembourgish'\n'mk': 'macedonian'\n'mg': 'malagasy'\n'ms': 'malay'\n'ml': 'malayalam'\n'mt': 'maltese'\n'mi': 'maori'\n'mr': 'marathi'\n'mn': 'mongolian'\n'my': 'myanmar (burmese)'\n'ne': 'nepali'\n'no': 'norwegian'\n'ps': 'pashto'\n'fa': 'persian'\n'pl': 'polish'\n'pt': 'portuguese'\n'pa': 'punjabi'\n'ro': 'romanian'\n'ru': 'russian'\n'sm': 'samoan'\n'gd': 'scots gaelic'\n'sr': 'serbian'\n'st': 'sesotho'\n'sn': 'shona'\n'sd': 'sindhi'\n'si': 'sinhala'\n'sk': 'slovak'\n'sl': 'slovenian'\n'so': 'somali'\n'es': 'spanish'\n'su': 'sundanese'\n'sw': 'swahili'\n'sv': 'swedish'\n'tg': 'tajik'\n'ta': 'tamil'\n'te': 'telugu'\n'th': 'thai'\n'tr': 'turkish'\n'uk': 'ukrainian'\n'ur': 'urdu'\n'uz': 'uzbek'\n'vi': 'vietnamese'\n'cy': 'welsh'\n'xh': 'xhosa'\n'yi': 'yiddish'\n'yo': 'yoruba'\n'zu': 'zulu'\n'fil': 'Filipino'\n'he': 'Hebrew'")

@app.on_message(Filters.text & Filters.private)
def echo(client, message):
    client.send_chat_action(message.chat.id, 'typing')
    snt_msg = client.send_message(message.chat.id, "**Processing...**", reply_to_message_id = message.message_id)
    msg = message.text
    t = msg.split(' | ')
    txt = str(t[0])
    if(len(t)>1):
        lan = str(t[1])
    else:
        lan = "en"
    translated = translate(txt,lan,"auto")
    print(translated)
    #print(tr.get_langs_list())
    client.delete_messages(message.chat.id,[snt_msg.message_id],True)
    client.send_chat_action(message.chat.id, 'typing')
    client.send_message(message.chat.id,"**Source text = **`{}`\n\n\n**Translated Text = **`{}`\n\n\n**Translated with **@lang_translate_bot".format(txt,translated),reply_to_message_id = message.message_id,reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(">> ⚠ Click here ⚠ <<", url="https://telegram.me/storebot?start=lang_translate_bot")],[InlineKeyboardButton("✋ Say Hi to Dev. ✋", url="https://t.me/odysseusmax")]]))
    

app.run()